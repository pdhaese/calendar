<?php
require 'database.php';
header("Content-Type: application/json"); // Since we are sending a JSON response here (not an HTML document), set the MIME Type to application/json

$username = $_POST['username'];
$password = $_POST['password'];


$stmt = $mysqli->prepare("SELECT username, password FROM users WHERE username=?");

$stmt->bind_param('s', $username);
$stmt->execute();

//Bind the results
$stmt->bind_result($u, $pwd_hash);
$stmt->fetch();

$pwd_guess = $_POST['password'];

if(password_verify($pwd_guess, $pwd_hash)) {
	//Login succeeded!
  ini_set("session.cookie_httponly", 1);
  session_start();
	$_SESSION['username'] = $username;
	//$_SESSION['token'] = substr(md5(rand()), 0, 10);
  $_SESSION['token'] = bin2hex(openssl_random_pseudo_bytes(32));

	echo json_encode(array(
		"success" => true,
    "token" => $_SESSION['token']
	));
	exit;

} else {
  echo json_encode(array(
		"success" => false,
		"message" => "Incorrect Username or Password"
	));
	exit;

}

?>
