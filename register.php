<?php
require 'database.php';

header("Content-Type: application/json"); // Since we are sending a JSON response here (not an HTML document), set the MIME Type to application/json

$username = $_POST['username'];
$password = $_POST['password'];

$passhash = password_hash($password,PASSWORD_BCRYPT);
$stmt = $mysqli->prepare("SELECT username FROM users WHERE username=?");

$stmt->bind_param('s', $username);
$stmt->execute();

//Bind the results
$stmt->bind_result($u);
$stmt->fetch();

if($u==null){
	$stmt->close();
	$query = $mysqli->prepare("insert into users (username, password) values (?, ?)");
	// if(!$stmt){
	// 	printf("Query Prep Failed: %s\n", $mysqli->error);
	// 	exit;
	// }
	$query->bind_param('ss', $username,$passhash);
	$query->execute();
	$query->close();
  echo json_encode(array(
    "success" => true
  ));
  exit;
} else {
  echo json_encode(array(
		"success" => false,
		"message" => "Username taken!"
	));
	exit;
}
?>
