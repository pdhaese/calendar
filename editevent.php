<?php
require 'database.php';
ini_set("session.cookie_httponly", 1);
session_start();
header("Content-Type: application/json"); // Since we are sending a JSON response here (not an HTML document), set the MIME Type to application/json

$id = $_POST['id'];
$title = $_POST['title'];
$editdate = $_POST['editdate'];
$edittime = $_POST['edittime'];

if(!hash_equals($_SESSION['token'], $_POST['token'])){
	die("Request forgery detected");
}

$query = $mysqli->prepare("UPDATE events SET title=?, eventdate=?, eventtime=? WHERE eventid=?");
//if(!$stmt){
// printf("Query Prep Failed: %s\n", $mysqli->error);
// exit;
// }
//if(){
$query->bind_param('ssss', $title, $editdate, $edittime, $id);
$query->execute();
$query->close();
  echo json_encode(array(
    "success" => true
  ));
  exit;

?>
