<?php
require 'database.php';
header("Content-Type: application/json"); // Since we are sending a JSON response here (not an HTML document), set the MIME Type to application/json

$username = $_POST['username'];
$title = $_POST['title'];
$date = $_POST['date'];
$time = $_POST['time'];
$rec = $_POST['receivers'];

$receivers= explode(",", $rec);

//need to combine date and time
$stmt = $mysqli->prepare("SELECT id FROM users WHERE username=?");
if(!$stmt){
printf("Query Prep Failed: %s\n", $mysqli->error);
exit;
}

$stmt->bind_param('s', $username);

$stmt->execute();

$stmt->bind_result($result);

$stmt->fetch();
$userid = $result;
$stmt->close();

$query = $mysqli->prepare("INSERT into events (title, eventdate, eventtime, userid) values (?,?,?,?)");
//if(!$stmt){
// printf("Query Prep Failed: %s\n", $mysqli->error);
// exit;
// }
//if(){
$query->bind_param('ssss', $title, $date, $time, $userid);
$query->execute();
$query->close();


for($i=0; $i<sizeof($receivers);$i++){
  $sq = $mysqli->prepare("SELECT id FROM users WHERE username=?");
  //need to combine date and time
  $rec = $receivers[$i];
  $receiverusername = $rec;

  if(!$sq){
  printf("Query Prep Failed: %s\n", $mysqli->error);
  exit;
  }

  $sq->bind_param('s', $receiverusername);

  $sq->execute();

  $sq->bind_result($r);

  $sq->fetch();
  $receiverid = $r;
  $sq->close();

  $nq = $mysqli->prepare("INSERT into events (title, eventdate, eventtime, userid) values (?,?,?,?)");
  //if(!$stmt){
  // printf("Query Prep Failed: %s\n", $mysqli->error);
  // exit;
  // }
  //if(){
  $nq->bind_param('ssss', $title, $date, $time, $receiverid);
  $nq->execute();
  $nq->close();
}

  echo json_encode(array(
    "success" => true,
    "receivers" => $receivers
  ));
  exit;
// } else {
//   echo json_encode(array(
// "success" => false,
// "message" => "Add event fail"
// ));
// exit;

?>
