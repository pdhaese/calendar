<?php
require 'database.php';
header("Content-Type: application/json"); // Since we are sending a JSON response here (not an HTML document), set the MIME Type to application/json
ini_set("session.cookie_httponly", 1);
session_start();

$username = $_SESSION['username'];
$begindate = $_POST['dates'];
//$begindate = strtotime($date);
date_default_timezone_set('UTC');
//string
$date = strtotime($begindate);
$ed = date("Y-m-d", strtotime("+1 month", $date));

//$ed = date("Y-m-d H:i:s", strtotime("+1 month", $begindate));

//datetime object
//$enddate = date_create_from_format('Y-m-d H:i:s', $ed, $timezone);
//$enddate_str = $enddate->format('Y-m-d');
//date_format($enddate,"Y-m-d");

$eventarray = array();
class event {
    public $title;
    public $date;
    public $time;
    public $id;
}


//need to combine date and time
$stmt = $mysqli->prepare("SELECT id FROM users WHERE username=?");
if(!$stmt){
printf("Query Prep Failed: %s\n", $mysqli->error);
exit;
}

$stmt->bind_param('s', $username);

$stmt->execute();

$stmt->bind_result($result);

$stmt->fetch();
$userid = $result;
$stmt->close();



$query = $mysqli->prepare("SELECT title, eventdate, eventtime, eventid FROM events WHERE userid=? AND eventdate >= ? AND eventdate < ?");
//if(!$stmt){
// printf("Query Prep Failed: %s\n", $mysqli->error);
// exit;
// }
//if(){
$query->bind_param('sss', $userid, $begindate,$ed);
$query->execute();
$query->bind_result($resultTitle,$resultDate,$resultTime, $resultid);
while($query->fetch()){
  $obj = new event();
  $obj->title = htmlentities($resultTitle);
  $obj->date = htmlentities($resultDate);
  $obj->time = htmlentities($resultTime);
  $obj->id = htmlentities($resultid);
  array_push($eventarray,$obj);

}

$query->close();
  echo json_encode(array(
    "success" => true,
    "results" => $eventarray
  ));
  exit;
// } else {
//   echo json_encode(array(
// "success" => false,
// "message" => "Add event fail"
// ));
// exit;

?>
