<?php
require 'database.php';
ini_set("session.cookie_httponly", 1);
session_start();

header("Content-Type: application/json"); // Since we are sending a JSON response here (not an HTML document), set the MIME Type to application/json

$sharereceiver = $_POST['sharereceiver'];
$user = $_SESSION['username'];

class event {
    public $title;
    public $date;
    public $time;
    //public $id;
}

//get the current user's id
$stmt = $mysqli->prepare("SELECT id FROM users WHERE username=?");
if(!$stmt){
printf("Query Prep Failed: %s\n", $mysqli->error);
exit;
}

$stmt->bind_param('s', $user);

$stmt->execute();

$stmt->bind_result($result);

$stmt->fetch();
$userid = $result;
$stmt->close();

$query = $mysqli->prepare("SELECT id FROM users WHERE username=?");
if(!$query){
printf("Query Prep Failed: %s\n", $mysqli->error);
exit;
}

$query->bind_param('s', $sharereceiver);

$query->execute();

$query->bind_result($got);

$query->fetch();
$receiverid = $got;
$query->close();

//Get all of my (current user's) events and save them in an array called sharearray
$ask = $mysqli->prepare("SELECT title, eventdate, eventtime FROM events WHERE userid=?");

$ask->bind_param('s', $userid);
$ask->execute();
$ask->bind_result($resultTitle,$resultDate,$resultTime);
$sharearray = array();


while($ask->fetch()){
  $obj = new event();
  $obj->title = $resultTitle;
  $obj->date = $resultDate;
  $obj->time = $resultTime;
//  $obj->id = $resultid;
  array_push($sharearray,$obj);
}

$ask->close();

//Now we'll input all these events into the events table on our database for and assign them to the user with whom i want to share my calendar
$addquery = $mysqli->prepare("INSERT into events (title, eventdate, eventtime, userid) values (?,?,?,?)");
if(!$addquery){
printf("Query Prep Failed: %s\n", $mysqli->error);
exit;
}
for($i=0; $i<sizeof($sharearray); $i++){
  $shareTitle = $sharearray[$i]->title;
  $shareDate = $sharearray[$i]->date;
  $shareTime = $sharearray[$i]->time;
  $addquery->bind_param('ssss', $shareTitle, $shareDate, $shareTime, $receiverid);
  $addquery->execute();

}
//if(){

$addquery->close();

echo json_encode(array(
  "success" => true
));
exit;


?>
