<?php
require 'database.php';
header("Content-Type: application/json"); // Since we are sending a JSON response here (not an HTML document), set the MIME Type to application/json

ini_set("session.cookie_httponly", 1);
session_start();
$thisuser = $_SESSION['username'];


$squery = $mysqli->prepare("SELECT username FROM users WHERE username!=?");

$recipients = array();
$squery->bind_param('s', $thisuser);
$squery->execute();
$squery->bind_result($recipient);
while($squery->fetch()){
  array_push($recipients,htmlentities($recipient));

}

$squery->close();

  echo json_encode(array(
    "success" => true,
    "results" => $recipients
  ));
  exit;

?>
