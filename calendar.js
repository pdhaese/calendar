	document.getElementById("event").hidden = true;
	document.getElementById("sharecalendar").hidden = true;
	document.getElementById("delete").hidden = true;
	document.getElementById("edit").hidden = true;
	document.getElementById("groupevent").hidden = true;
	document.getElementById("welcome").hidden = true;
	var currentusername = null;
	var isUserLoggedIn = false;
	var eventids = [];


function clockTick() {
	  var currentTime = new Date();
	  var month = currentTime.getMonth() + 1;
	  var day = currentTime.getDate();
	  var year = currentTime.getFullYear();
	  var hours = currentTime.getHours();
	  var minutes = currentTime.getMinutes();
	  var seconds = currentTime.getSeconds();
		if(hours === 0){
			hours = 12;
		}
		if(hours > 12){
			hours = hours - 12;
		}
	   var datetext = month + "/" + day + "/" + year + ' ';
		 var timetext = hours + ':' + minutes + ':' + seconds;
	  // here we get the element with the id of "date" and change the content to the text variable we made above
	  document.getElementById("displaydate").textContent = datetext;
		document.getElementById("displaytime").textContent = timetext;
	}

	// here we run the clockTick function every 1000ms (1 second)
	setInterval(clockTick, 1000);

function loginAjax(event){
	var username = document.getElementById("username").value; // Get the username from the form
	var password = document.getElementById("password").value; // Get the password from the form

	// Make a URL-encoded string for passing POST data:
	var dataString = "username=" + encodeURIComponent(username) + "&password=" + encodeURIComponent(password);

	var xmlHttp = new XMLHttpRequest(); // Initialize our XMLHttpRequest instance
	xmlHttp.open("POST", "login.php", true); // Starting a POST request (NEVER send passwords as GET variables!!!)
	xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"); // It's easy to forget this line for POST requests
	xmlHttp.addEventListener("load", function(event){
		var jsonData = JSON.parse(event.target.responseText); // parse the JSON into a JavaScript object
		if(jsonData.success){  // in PHP, this was the "success" key in the associative array; in JavaScript, it's the .success property of jsonData
			alert("You've been Logged In!");
				document.getElementById("deletetoken").value = jsonData.token;
				document.getElementById("edittoken").value = jsonData.token;
				document.getElementById("event").hidden = false;
				document.getElementById("sharecalendar").hidden = false;
				document.getElementById("delete").hidden = false;
				document.getElementById("edit").hidden = false;
				document.getElementById("groupevent").hidden = false;
				document.getElementById("welcome").hidden = false;
				isUserLoggedIn = true;
				getEventsAjax(event,"2017-10-01");
				currentusername = username;
				document.getElementById("welcome").textContent ="Welcome " +  currentusername + "!";
				displaySharers(event);
				var date = new Date();
				var month = date.getMonth();
				var year = date.getFullYear();
				calendar(month, year);


		}else{
			alert("You were not logged in.  "+jsonData.message);
		}
	}, false); // Bind the callback to the load event
	xmlHttp.send(dataString); // Send the data
}

function registerAjax(event){
	var username = document.getElementById("registerusername").value; // Get the username from the form
	var password = document.getElementById("registerpassword").value; // Get the password from the form

	// Make a URL-encoded string for passing POST data:
	var dataString = "username=" + encodeURIComponent(username) + "&password=" + encodeURIComponent(password);

	var xmlHttp = new XMLHttpRequest(); // Initialize our XMLHttpRequest instance
	xmlHttp.open("POST", "register.php", true); // Starting a POST request (NEVER send passwords as GET variables!!!)
	xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"); // It's easy to forget this line for POST requests
	xmlHttp.addEventListener("load", function(event){
		var jsonData = JSON.parse(event.target.responseText); // parse the JSON into a JavaScript object
		if(jsonData.success){  // in PHP, this was the "success" key in the associative array; in JavaScript, it's the .success property of jsonData
			alert("You've been Registered! You may now login");
		}else{
			alert("You were not registered.  "+jsonData.message);
		}
	}, false); // Bind the callback to the load event
	xmlHttp.send(dataString); // Send the data
}

function getEventsAjax(event, geteventdate){

	// Make a URL-encoded string for passing POST data:
	var dataString = "dates=" + encodeURIComponent(geteventdate);

	var xmlHttp = new XMLHttpRequest(); // Initialize our XMLHttpRequest instance
	xmlHttp.open("POST", "getevents.php", true); // Starting a POST request (NEVER send passwords as GET variables!!!)
	xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"); // It's easy to forget this line for POST requests
	xmlHttp.addEventListener("load", function(event){
		var jsonData = JSON.parse(event.target.responseText); // parse the JSON into a JavaScript object
		if(jsonData.success){  // in PHP, this was the "success" key in the associative array; in JavaScript, it's the .success property of jsonData

			listevents(jsonData.results);
		}else{
			alert("Could not retrieve events "+jsonData.message);
		}
	}, false); // Bind the callback to the load event
	window.load = xmlHttp.send(dataString); // Send the data

}

//dislay everyone that I can share my calendar or an event with (all users except myself)
function displaySharers(event){
	// // Make a URL-encoded string for passing POST data:
	var dataString = "username=" + encodeURIComponent(currentusername);

	var xmlHttp = new XMLHttpRequest(); // Initialize our XMLHttpRequest instance
	xmlHttp.open("POST", "displaysharers.php", true); // Starting a POST request (NEVER send passwords as GET variables!!!)
	xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"); // It's easy to forget this line for POST requests
	xmlHttp.addEventListener("load", function(event){
		var jsonData = JSON.parse(event.target.responseText); // parse the JSON into a JavaScript object
		if(jsonData.success){  // in PHP, this was the "success" key in the associative array; in JavaScript, it's the .success property of jsonData
			var recipients = jsonData.results;
			var dropdown = "";
			for(var i = 0; i<recipients.length; i++){
				dropdown += "<option value ='" + recipients[i] + "'>" + recipients[i] + "</option>";
			}
				//alert("You've been Logged In!");
			document.getElementById("recipient").innerHTML = dropdown;
			document.getElementById("grouprecipient").innerHTML = dropdown;


		}else{
			//alert("You were not logged in.  "+jsonData.message);
		}
	}, false); // Bind the callback to the load event
	xmlHttp.send(dataString); // Send the data
}

function shareMyCalendar(event){
	var sharereceiver = document.getElementById("recipient").value; // Get the username from the form
	// var password = document.getElementById("password").value; // Get the password from the form
	//
	// // Make a URL-encoded string for passing POST data:
	var dataString = "sharereceiver=" + encodeURIComponent(sharereceiver);

	var xmlHttp = new XMLHttpRequest(); // Initialize our XMLHttpRequest instance
	xmlHttp.open("POST", "sharecalendar.php", true); // Starting a POST request (NEVER send passwords as GET variables!!!)
	xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"); // It's easy to forget this line for POST requests
	xmlHttp.addEventListener("load", function(event){
		var jsonData = JSON.parse(event.target.responseText); // parse the JSON into a JavaScript object
		if(jsonData.success){  // in PHP, this was the "success" key in the associative array; in JavaScript, it's the .success property of jsonData
			alert("Shared!");

		}else{
			alert("Could not share your calendar");

		}
	}, false); // Bind the callback to the load event
	xmlHttp.send(dataString); // Send the data
}

//found rough calendar function online at https://stackoverflow.com/questions/17391886/a-plugin-to-create-html-calendars-tables-easily
// We used there structure as the base for the navigation through dates within our app
function calendar(month, year) {
	document.getElementById("calendar").innerHTML = "";
    //Variables to be used later.  Place holders right now.
    var padding = "";
    var totalFeb = "";
    var i = 1;

    var current = new Date();
    var cmonth = current.getMonth();
    var day = current.getDate();
    //var year = current.getFullYear();
    var tempMonth = month + 1; //+1; //Used to match up the current month with the correct start date.

    //Determing if Feb has 28 or 29 days in it.
    if (month == 1) {
        if ((year % 100 !== 0) && (year % 4 === 0) || (year % 400 === 0)) {
            totalFeb = 29;
        } else {
            totalFeb = 28;
        }
    }

    // Setting up arrays for the name of the months, days, and the number of days in the month.
    var monthNames = ["Jan", "Feb", "March", "April", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec"];
    var totalDays = ["31", "" + totalFeb + "", "31", "30", "31", "30", "31", "31", "30", "31", "30", "31"];

    // Temp values to get the number of days in current month, and previous month. Also getting the day of the week.
    var tempDate = new Date(tempMonth + ' 1 ,' + year);
    var tempweekday = tempDate.getDay();
    var tempweekday2 = tempweekday;
    var dayAmount = totalDays[month];

    // After getting the first day of the week for the month, padding the other days for that week with the previous months days.  IE, if the first day of the week is on a Thursday, then this fills in Sun - Wed with the last months dates, counting down from the last day on Wed, until Sunday.
    while (tempweekday > 0) {
        padding += "<td class='premonth'></td>";
        //preAmount++;
        tempweekday--;
    }


    // Filling in the calendar with the current month days in the correct location along.
		//call get events
		//get results array

		var querymonth = month+1;
		var monthdigits = querymonth.toString().length;
		var GEdate;
		if(monthdigits == 1){
			GEdate = year + "-0" + querymonth + "-" + "01";
		}
		else{
			GEdate = year + "-" + querymonth + "-" + "01";
		}

    while (i <= dayAmount) {

        // Determining when to start a new row
        if (tempweekday2 > 6) {
            tempweekday2 = 0;
            padding += "</tr><tr>";
        }

        // checking to see if i is equal to the current day, if so then we are making the color of that cell a different color using CSS. Also adding a rollover effect to highlight the day the user rolls over. This loop creates the actual calendar that is displayed.
        if (i == day && month == cmonth) {

            padding += "<td class='currentday'  onMouseOver='this.style.background=\"#00FF00\"; this.style.color=\"#FFFFFF\"' onMouseOut='this.style.background=\"#FFFFFF\"; this.style.color=\"#00FF00\"'>" + i + "</td>";

			  } else {

            padding += "<td class='currentmonth' onMouseOver='this.style.background=\"#00FF00\"' onMouseOut='this.style.background=\"#FFFFFF\"'>" + i  + "</td>";
        }
        tempweekday2++;
        i++;
    }


    // Outputing the calendar onto the site.  Also, putting in the month name and days of the week.
    var calendarTable = "<table class='calendar'> <tr class='currentmonth'><th colspan='7'>" + monthNames[month] + " " + year + "</th></tr>";
    calendarTable += "<tr class='weekdays'>  <td>Sun</td>  <td>Mon</td> <td>Tues</td> <td>Wed</td> <td>Thurs</td> <td>Fri</td> <td>Sat</td> </tr>";
    calendarTable += "<tr>";
    calendarTable += padding;
    calendarTable += "</tr></table>";
    document.getElementById("calendar").innerHTML += calendarTable;
    document.getElementById("month").value = month;
    document.getElementById("year").value = year;
		if(currentusername !== null){
			getEventsAjax(event,GEdate);
			displaySharers(event);
		}

}
function listevents(eventsarray){
	document.getElementById("events").innerHTML = "";
	var eventTable = "<table class='events'> <tr class='currentmonth'><th colspan='7'>Your events for the month</th></tr>";
	var padding = "";
	eventids = [];
	for (var j = 0; j < eventsarray.length; j++){
		var par = eventsarray[j].date.toString();
		console.log(par);
		var thisdate = par.split("-");
		var str_thisdate = thisdate[1] + "/" + thisdate[2] + "/" + thisdate[0];
		padding += "<tr class='currentday'>" + eventsarray[j].title + "<br>" + str_thisdate + "<br>Time: " + eventsarray[j].time + "<br>" + "Event Id:" + eventsarray[j].id + "<br><br><br></tr>";
		eventids.push(eventsarray[j].id);
	}
	eventTable += padding;
	eventTable += "</table>";
	document.getElementById("events").innerHTML = eventTable;
}



function prevmonth(){
	document.getElementById("calendar").innerHTML = "";
  month = parseInt(document.getElementById("month").value);
  year = parseInt(document.getElementById("year").value);
	var newmonth;
	var newyear;
  if(month === 0){
    newmonth = 11;
    newyear = year -1;
  } else {
    newmonth = month -1;
    newyear = year;
  }
  calendar(newmonth, newyear);
}

function nextmonth(){
	document.getElementById("calendar").innerHTML = "";
	month = parseInt(document.getElementById("month").value);
	year = parseInt(document.getElementById("year").value);
	var newmonth;
	var newyear;
	if(month == 11){
		newmonth = 0;
		newyear = year +1;
	} else {
		newmonth = month +1;
		newyear = year;
	}
	calendar(newmonth, newyear);
}

function addEvent(event){
//get current username of person logged in--> global variable current username
var title = document.getElementById("title").value; // Get the info from the form
var date = document.getElementById("date").value;
var time = document.getElementById("time").value;
// Make a URL-encoded string for passing POST data:
var dataString = "username=" + encodeURIComponent(currentusername) + "&title=" + encodeURIComponent(title) + "&date=" + encodeURIComponent(date) + "&time=" + encodeURIComponent(time);

var xmlHttp = new XMLHttpRequest(); // Initialize our XMLHttpRequest instance
xmlHttp.open("POST", "addevent.php", true); // Starting a POST request (NEVER send passwords as GET variables!!!)
xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"); // It's easy to forget this line for POST requests
xmlHttp.addEventListener("load", function(event){
var jsonData = JSON.parse(event.target.responseText); // parse the JSON into a JavaScript object

if(jsonData.success){  // in PHP, this was the "success" key in the associative array; in JavaScript, it's the .success property of jsonData
alert("You added an event");
document.getElementById("event").hidden = false;
//var date = new Date();
//var month = date.getMonth();
//var year = date.getFullYear();
var date = new Date();
var month = date.getMonth();
var year = date.getFullYear();
calendar(month, year);
}else{
alert("Event add failed  "+jsonData.message);
}
}, false); // Bind the callback to the load event
xmlHttp.send(dataString); // Send the data

}

function addGroupEvent(event){
//get current username of person logged in--> global variable current username
var title = document.getElementById("grouptitle").value; // Get the info from the form
var date = document.getElementById("groupdate").value;
var time = document.getElementById("grouptime").value;
var g = document.getElementById("grouprecipient");
var receivers = [];
var count = 0;
for (var i = 0; i < g.options.length; i++) {
  	if (g.options[i].selected) {
    	count++;
   	 receivers.push(g.options[i].value);
 	 }
	}
	console.log(receivers);
var str_receivers = receivers.toString();
console.log("Str: " + str_receivers);
// Make a URL-encoded string for passing POST data:
var dataString = "username=" + encodeURIComponent(currentusername) + "&title=" + encodeURIComponent(title) + "&date=" + encodeURIComponent(date) + "&time=" + encodeURIComponent(time) + "&receivers=" + encodeURIComponent(str_receivers);

var xmlHttp = new XMLHttpRequest(); // Initialize our XMLHttpRequest instance
xmlHttp.open("POST", "addgroupevent.php", true); // Starting a POST request (NEVER send passwords as GET variables!!!)
xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"); // It's easy to forget this line for POST requests
xmlHttp.addEventListener("load", function(event){
var jsonData = JSON.parse(event.target.responseText); // parse the JSON into a JavaScript object

if(jsonData.success){  // in PHP, this was the "success" key in the associative array; in JavaScript, it's the .success property of jsonData
alert("You added a group event");
document.getElementById("event").hidden = false;
console.log("receivers" + jsonData.results);
//var date = new Date();
//var month = date.getMonth();
//var year = date.getFullYear();
var date = new Date();
var month = date.getMonth();
var year = date.getFullYear();
calendar(month, year);
}else{
alert("Event add failed  "+jsonData.message);
}
}, false);
xmlHttp.send(dataString); // Send the data

}

function editEvent(event){
	//get current username of person logged in--> global variable current username
	var edittitle = document.getElementById("edittitle").value; // Get the info from the form
	var editdate = document.getElementById("editdate").value;
	var edittime = document.getElementById("edittime").value;
	var editid = document.getElementById("editid").value;
	var token = document.getElementById("edittoken").value;
	var valid = false;
	var mytest = parseInt(editid);
	for(var j = 0; j <eventids.length;j++){
		if(mytest == parseInt(eventids[j])){
			valid = true;
		}
	}
	if(valid){
		var dataString = "title=" + encodeURIComponent(edittitle) + "&editdate=" + encodeURIComponent(editdate) + "&edittime=" + encodeURIComponent(edittime) + "&id=" + encodeURIComponent(editid) + "&token=" + encodeURIComponent(token);

		var xmlHttp = new XMLHttpRequest(); // Initialize our XMLHttpRequest instance
		xmlHttp.open("POST", "editevent.php", true); // Starting a POST request (NEVER send passwords as GET variables!!!)
		xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"); // It's easy to forget this line for POST requests
		xmlHttp.addEventListener("load", function(event){
			var jsonData = JSON.parse(event.target.responseText); // parse the JSON into a JavaScript object
			if(jsonData.success){  // in PHP, this was the "success" key in the associative array; in JavaScript, it's the .success property of jsonData
				alert("You edited an event");
				document.getElementById("event").hidden = false;
				var date = new Date();
				var month = date.getMonth();
				var year = date.getFullYear();
				calendar(month, year);
			}else{
				alert("Event edit failed  "+jsonData.message);
			}
	}, false); // Bind the callback to the load event
	xmlHttp.send(dataString); // Send the data
} else {
	alert("please input a valid eventid")
}
}

function deleteEvent(){
	var deleteid = parseInt(document.getElementById("deleteid").value);
	var token = document.getElementById("deletetoken").value;
	var valid = false;
	for(var j = 0; j <eventids.length;j++){
		if(deleteid == eventids[j]){
			valid = true;
		}
	}
	if(valid){
		var dataString = "id=" + encodeURIComponent(deleteid) + "&token=" + encodeURIComponent(token);
		var xmlHttp = new XMLHttpRequest(); // Initialize our XMLHttpRequest instance
		xmlHttp.open("POST", "deleteevent.php", true); // Starting a POST request (NEVER send passwords as GET variables!!!)
		xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"); // It's easy to forget this line for POST requests
		xmlHttp.addEventListener("load", function(event){
			var jsonData = JSON.parse(event.target.responseText); // parse the JSON into a JavaScript object

			if(jsonData.success){  // in PHP, this was the "success" key in the associative array; in JavaScript, it's the .success property of jsonData
				alert("You successfully deleted an event");
				document.getElementById("event").hidden = false;
				var date = new Date();
				var month = date.getMonth();
				var year = date.getFullYear();
				calendar(month, year);
			}else{
				alert("Event deletion failed"+jsonData.message);
			}
		}, false); // Bind the callback to the load event
		xmlHttp.send(dataString); // S
	} else {
		alert("The event id you entered was not a valid event id");
	}
}

document.getElementById("login_btn").addEventListener("click", loginAjax, false);
document.getElementById("register_btn").addEventListener("click", registerAjax, false);
document.getElementById("next").addEventListener("click", nextmonth, false);
document.getElementById("prev").addEventListener("click", prevmonth, false);
document.getElementById("addevent_btn").addEventListener("click", addEvent, false);
document.getElementById("deleteevent").addEventListener("click", deleteEvent, false);
document.getElementById("editevent").addEventListener("click", editEvent, false);
document.getElementById("share_btn").addEventListener("click", shareMyCalendar, false);
document.getElementById("addgroupevent_btn").addEventListener("click", addGroupEvent, false);

var date = new Date();
var month = date.getMonth();
var year = date.getFullYear();
calendar(month, year);
