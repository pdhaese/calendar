<?php
require 'database.php';
ini_set("session.cookie_httponly", 1);
session_start();
header("Content-Type: application/json"); // Since we are sending a JSON response here (not an HTML document), set the MIME Type to application/json

$id = $_POST['id'];
if(!hash_equals($_SESSION['token'], $_POST['token'])){
	die("Request forgery detected");
}

$stmt = $mysqli->prepare("DELETE FROM events WHERE eventid=?");

$stmt->bind_param('s', $id);
$stmt->execute();
$stmt->close();
echo json_encode(array(
  "success" => true
));
exit;

?>
